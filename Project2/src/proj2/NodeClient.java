package proj2;
import java.io.*;
import java.util.*;
import java.net.*;

public class NodeClient {
	private static String connected_host = "";
	private static int connected_port = 0;
	
	private static Set<String> getTypeSet() {
		Set<String> type_set = new HashSet<String>();
		ParcelType[] types = ParcelType.values();
		for (ParcelType type : types) {
			type_set.add(type.toString());
		}
		return type_set;
	}
	
	public static void main(String[] args) {
		if (args.length == 2) {
			connected_host = args[0];
			connected_port = Integer.parseInt(args[1]);
		}
		
		if (connected_host.equals("") || connected_port == 0) {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Please enter server host: ");
			connected_host = scanner.nextLine();
			System.out.print("Please enter server port: ");
			connected_port = Integer.parseInt(scanner.nextLine());
			scanner.close();
		}
		
		Scanner scanner = null;
		Socket clientSocket = null;
		ObjectOutput out = null;
		try {
			clientSocket = new Socket(connected_host, connected_port);
			scanner = new Scanner(System.in);
			System.out.print("Please enter operation type: ");
			String op_type = scanner.nextLine();
			Set<String> type_set = getTypeSet();
			
			if (type_set.contains(op_type)) {
				ParcelType type = ParcelType.valueOf(op_type);
				ParcelInfo info = new ParcelInfo(0, 0);
				System.out.print("Please enter the message: ");
				String text = scanner.nextLine();
				Message message = new Message(0, text, type);
				Parcel parcel = new Parcel(ParcelType.CMD, info, message);
				out = new ObjectOutputStream(clientSocket.getOutputStream());
				out.writeObject(parcel);
				out.flush();
			}
			else {
				System.out.printf("The input operation type %s is not found!!\n", op_type);
			}
		}
		catch (IOException ex) {}
		finally {
			if (scanner != null) {
				scanner.close();
			}
			
			if (clientSocket != null) {
				try {
					clientSocket.close();
				} catch (IOException e) {
					//Should we ignore the exception when close a client socket?
					e.printStackTrace();
				}
			}
			
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					//Should we ignore the exception when close a output stream
					e.printStackTrace();
				}
			}
		}
	}//end of main()
}
