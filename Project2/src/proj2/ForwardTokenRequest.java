package proj2;

public class ForwardTokenRequest implements TokenRequest {
	private int fromNodeNum;
	
	ForwardTokenRequest(int fromNodeNum) {
		this.fromNodeNum = fromNodeNum;
	}
	
	public int getNewTokenParent() {
		return fromNodeNum;
	}
	
	@Override
	public void response(ServerContext ctx) {
		String fromNodeHost = ctx.getNodeHost(fromNodeNum);
		int fromNodePort = ctx.getNodePort(fromNodeNum);
		
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		
		String text = String.format("[SysMutex] Forwarding token to Node %d(%s:%d) from Node %d(%s:%d)\n", 
				fromNodeNum, fromNodeHost, fromNodePort,
				currentNodeNum, currentNodeHost, currentNodePort);
		NodeServer.log(ctx, text);
		
		/* If this method is called, means that we have token right now, but we are not going to use it */
		RaymondTokenManager tokenManager = ctx.getTokenManager();
		//Thread.yield();
		tokenManager.forwardToken(fromNodeNum);
	}

}
