package proj2;

public class ReceiveShutdownAction extends AbstractReceiveBroadcastAction{

	ReceiveShutdownAction(ServerContext ctx) {
		this.ctx = ctx;
		this.outgoingType = ParcelType.SHUTDOWN_ACK;
	}
	
	@Override
	protected void doAfterAckUpStream() {
		ctx.getHandler().stopServer();
	}

	@Override
	protected void doBeforBroadCast(Conversation conversation, ParcelInfo info) {}
}
