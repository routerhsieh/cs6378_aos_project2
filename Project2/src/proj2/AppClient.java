package proj2;

import java.io.*;
import java.net.*;
import java.util.*;

public class AppClient {
	private static String connected_host = "";
	private static int connected_port = 0;
	
	private static Set<String> getTypeSet() {
		Set<String> type_set = new HashSet<String>();
		ApplicationType[] types = ApplicationType.values();
		for (ApplicationType type : types) {
			type_set.add(type.toString());
		}
		return type_set;
	}
	
	public static void main(String[] args) {
		if (args.length >= 2) {
			connected_host = args[0];
			connected_port = Integer.parseInt(args[1]);
		}
		
		if (connected_host.equals("") || connected_port == 0) {
			Scanner scanner = new Scanner(System.in);
			System.out.print("Please enter server host: ");
			connected_host = scanner.nextLine();
			System.out.print("Please enter server port: ");
			connected_port = Integer.parseInt(scanner.nextLine());
			scanner.close();
		}
		
		Scanner scanner = null;
		Socket clientSocket = null;
		ObjectOutput out = null;
		String app_type = null;
		String text = null;
		try {
			clientSocket = new Socket(connected_host, connected_port);
			Set<String> type_set = getTypeSet();
			if (args.length <= 2) {
			scanner = new Scanner(System.in);
			System.out.print("Please enter application type: ");
			app_type = scanner.nextLine();
			}
			else {
				app_type = args[2];
			}	
			
			if (type_set.contains(app_type)) {
				ApplicationType appType = null;
				for (ApplicationType type : ApplicationType.values()) {
					if (type.toString().equals(app_type)) {
						appType = type;
						break;
					}
				}
				ParcelInfo info = new ParcelInfo(0, 0);
				if (args.length <= 3) {
				System.out.print("Please enter the message: ");
				text = scanner.nextLine();
				}
				else {
					text = args[3];
				}
				Message message = new Message(text, appType);
				Parcel parcel = new Parcel(ParcelType.APP, info, message);
				out = new ObjectOutputStream(clientSocket.getOutputStream());
				out.writeObject(parcel);
				out.flush();
			}
			else {
				System.out.printf("The input application type %s is not found!!\n", app_type);
				System.out.println("Possible applications: ");
				for (String typeName : type_set) {
					System.out.printf("%s\n", typeName);
				}
			}
		}
		catch (IOException ex) {}
		finally {
			if (scanner != null) {
				scanner.close();
			}
			
			if (clientSocket != null) {
				try {
					clientSocket.close();
				} catch (IOException e) {
					//Should we ignore the exception when close a client socket?
					e.printStackTrace();
				}
			}
			
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					//Should we ignore the exception when close a output stream
					e.printStackTrace();
				}
			}
		}
	}//end of main()
}
