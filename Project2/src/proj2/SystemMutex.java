package proj2;

/**
 * 1. Provide the enterCS() and leaveCS() blocking API for application module
 * 2. Anyone call enterCS() should be blocked unitl it can get the token/semaphore
 * 3. The token/semaphore should be release after leaving leaveCS()
 * @author router
 *
 */

public interface SystemMutex {
	public void enter();
	public void leave();
}
