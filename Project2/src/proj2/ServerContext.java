package proj2;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Every method in this class should be thread-safe
 * @author router
 *
 */

public class ServerContext {
	private Commander commander = new Commander();
	private SystemMutex sysMutex;
	private RaymondTokenManager tokenManager;
	private ParcelHandler handler = null;
	private int nodeNum;
	private HashMap<Integer, NodeInfo> nodeInfos = null;
	private final Map<UUID, Conversation> conversationStorage = new ConcurrentHashMap<UUID, Conversation>();
	volatile private boolean inTree = false;
	private String outputFilePath = null;
	private int autoCSRequirements = 1000;
	private int meanCSRequirementInterval = 50;
	private int meanCSRunningInterval = 10;
	private String outputFilePathCriticalSection = "critical_sections.txt";

	ServerContext() {
	}

	ServerContext(ParcelHandler handler, int nodeNum,
			HashMap<Integer, NodeInfo> nodeInfos, String outputFilePath) {
		this.handler = handler;
		this.setCurrentNodeNum(nodeNum);
		this.setNodeInfos(nodeInfos);
		this.setOutputFilePath(outputFilePath);
	}

	public Commander getCommander() {
		return this.commander;
	}
	
	public void setSysMutex(SystemMutex sysMutex) {
		this.sysMutex = sysMutex;
	}
	
	public SystemMutex getSysMutex() {
		return this.sysMutex;
	}

	public void setHandler(ParcelHandler handler) {
		this.handler = handler;
	}

	public ParcelHandler getHandler() {
		return this.handler;
	}

	public int getCurrentNodeNum() {
		return nodeNum;
	}

	public void setCurrentNodeNum(int nodeNum) {
		this.nodeNum = nodeNum;
	}
	
	public String getCurrentHost() {
		NodeInfo node = nodeInfos.get(nodeNum);
		return node.getHost();
	}
	
	public int getCurrentPort() {
		NodeInfo node = nodeInfos.get(nodeNum);
		return node.getPort();
	}

	public HashMap<Integer, NodeInfo> getNodeInfos() {
		return nodeInfos;
	}

	public void setNodeInfos(HashMap<Integer, NodeInfo> nodeInfos) {
		this.nodeInfos = nodeInfos;
	}

	public NodeInfo getCurrentNodeInfo() {
		NodeInfo node = nodeInfos.get(nodeNum);
		return node;
	}
	
	public NodeInfo getNodeInfo(int nodeNum) {
		return nodeInfos.get(nodeNum);
	}
	
	public String getNodeHost(int nodeNum) {
		return nodeInfos.get(nodeNum).getHost();
	}
	
	public int getNodePort(int nodeNum) {
		return nodeInfos.get(nodeNum).getPort();
	}

	public Set<Integer> getPhysicalNeighbors() {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getPhysicalNeighbors();
		return neighbors;
	}

	public Set<Integer> getLogicalNeighbors() {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getLogicalNeighbors();
		return neighbors;
	}
	
	public void setLogicalNeighbor(int neighborNodeId) {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getLogicalNeighbors();
		neighbors.add(neighborNodeId);
	}
	
	public void clearLogicalNeighbors() {
		NodeInfo node = nodeInfos.get(nodeNum);
		Set<Integer> neighbors = node.getLogicalNeighbors();
		neighbors.clear();
	}
	
	public synchronized boolean isInTree() {
		if (!this.inTree) {
			this.inTree = true;
			return false;
		}
		else {
			return true;
		}
	}
	

	public void setInTree(boolean inTree) {
		this.inTree = inTree;
	}

	public Conversation getConversation(UUID uuid) {
		return conversationStorage.get(uuid);
	}
	
	public Conversation newConversation(UUID uuid) {
		Conversation conversation = conversationStorage.get(uuid);
		if (conversation != null) {
			throw new IllegalArgumentException("Duplicate UUID: " + uuid) ;
		}
		else {
			conversation = new Conversation(uuid);
			conversationStorage.put(uuid, conversation);
			return conversation;
		}
	}
	
	public Conversation beginConversation() {
		Conversation conversation = new Conversation();
		conversationStorage.put(conversation.getUuid(), conversation);
		return conversation;
	}
	
	public void endConversation(UUID uuid) {
		conversationStorage.remove(uuid);
	}

	public String getOutputFilePath() {
		return outputFilePath;
	}

	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}
	
	public RaymondTokenManager getTokenManager() {
		return tokenManager;
	}

	public void setTokenManager(RaymondTokenManager tokenManager) {
		this.tokenManager = tokenManager;
	}
	
	public int getAutoCSRequirements() {
		return autoCSRequirements;
	}

	public void setAutoCSRequirements(int autoCSRequirements) {
		this.autoCSRequirements = autoCSRequirements;
	}
	
	public int getMeanCSRequirementInterval() {
		return meanCSRequirementInterval;
	}

	public void setMeanCSRequirementInterval(int meanCSRequirementInterval) {
		this.meanCSRequirementInterval = meanCSRequirementInterval;
	}
	
	public int getMeanCSRunningInterval() {
		return meanCSRunningInterval;
	}

	public void setMeanCSRunningInterval(int meanCSRunningInterval) {
		this.meanCSRunningInterval = meanCSRunningInterval;
	}
	
	public int getTotalNodes() {
		return nodeInfos.size();
	}
	
	public String getOutputFilePathCriticalSection() {
		return outputFilePathCriticalSection;
	}

	public void setOutputFilePathCriticalSection(
			String outputFilePathCriticalSection) {
		this.outputFilePathCriticalSection = outputFilePathCriticalSection;
	}

}
