package proj2;

import java.util.Vector;

public interface RaymondTokenManager {
	
	void acquireToken(Vector<Integer> receivedVectorClock, int fromNodeNum);
	
	void claimToken(int claimerNodeNum, Vector<Integer> receivedVectorClock);
	
	void forwardToken(int dstNodeNum);
	
	void init(int ownerNodeNum);
	
	void setTokenUsing(boolean isUsing);
}