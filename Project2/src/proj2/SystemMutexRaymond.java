package proj2;

import java.util.Queue;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;

public class SystemMutexRaymond implements SystemMutex, RaymondTokenManager {

	private Semaphore csSem = new Semaphore(1, true);
	private ServerContext ctx;
	/* If we are the token owner, then set tokenParent to -1 */
	private volatile int tokenParent = -1;
	private volatile boolean isTokenOwner = false;
	private volatile boolean isTokenUsing = false;
	private volatile boolean sentBefore = false;
	private Queue<TokenRequest> tokenRequestQueue = new ConcurrentLinkedQueue<TokenRequest>();
	private Vector<Integer> vectorClock = null;
	
	SystemMutexRaymond(ServerContext ctx) {
		this.ctx = ctx;
		this.vectorClock = new Vector<Integer>(ctx.getTotalNodes());
		initialVectorClock();
	}
	
	private int getTokenParent() {
		return tokenParent;
	}

	private void setTokenParent(int tokenParent) {
		this.tokenParent = tokenParent;
	}

	private void setTokenOwner(boolean isOwn) {
		this.isTokenOwner = isOwn;
	}
	
	private boolean isSentBefore() {
		return sentBefore;
	}

	private void setSentBefore(boolean hasSentBefore) {
		this.sentBefore = hasSentBefore;
	}

	private void enQueue(TokenRequest request) {
		tokenRequestQueue.add(request);
	}
	
	private TokenRequest deQueue() {
		TokenRequest request = null;
		if (!tokenRequestQueue.isEmpty()) {
			request = tokenRequestQueue.remove();
		}
		else {
			request = null;
		}
		return request;
	}
	
	private void initialVectorClock() {
		for (int idx = 0; idx < ctx.getTotalNodes(); idx++) {
			vectorClock.add(0);
		}
	}
	
	private synchronized void incrementVectorClock() {
		int currentNodeNum = ctx.getCurrentNodeNum();
		int currentValue = vectorClock.get(currentNodeNum - 1);
		currentValue += 1;
		vectorClock.set(currentNodeNum - 1, currentValue);
	}
	
	private synchronized void mergeVectorClock(Vector<Integer> receivedVector) {
		/* vector's size should equal to total node numbers in system */
		int vectorSize = ctx.getTotalNodes();
		for (int idx = 0; idx < vectorSize; idx++) {
			int value = receivedVector.get(idx);
			if (value > vectorClock.get(idx)) {
				vectorClock.set(idx, value);
			}
		}
	}
	
	/* We will not allow the same node issue multiple token requests at same time */
	@Override
	public void enter() {
		try {
			csSem.acquire();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		CurrentTaskTokenRequest request = null;
		
		Thread.yield();
		synchronized (this) {
			if (!isTokenOwner) {
				String text = String
						.format("[SysMutex] Not token owner at Node %d(%s:%d), going to be blocked...\n",
								currentNodeNum, currentNodeHost,
								currentNodePort);
				NodeServer.log(ctx, text);

				request = new CurrentTaskTokenRequest(false);
				this.enQueue(request);
				Commander commander = ctx.getCommander();
				if (!isSentBefore()) {
					commander.requestToken(tokenParent, ctx, vectorClock);
					setSentBefore(true);
				}
			} else {
				setTokenUsing(true);
				setSentBefore(false);
			}
		}
		if (request != null) {
			request.await();
		}
		String text = String
				.format("[EnterCS] Is token owner at Node %d(%s:%d), going to enter critical section\n",
						currentNodeNum, currentNodeHost, currentNodePort);
		NodeServer.log(ctx, text);
		incrementVectorClock();
		@SuppressWarnings("unchecked")
		Vector<Integer> currentVectorClock = (Vector<Integer>) vectorClock.clone();
		NodeServer.logCriticalSection(ctx, text, currentVectorClock);
	}

	@Override
	public void leave() {
		Thread.yield();
		synchronized (this) {
			setTokenUsing(false);

			int currentNodeNum = ctx.getCurrentNodeNum();
			String currentNodeHost = ctx.getCurrentHost();
			int currentNodePort = ctx.getCurrentPort();
			String text = String.format(
					"[LeaveCS] Going to leave critical at Node %d(%s:%d)\n",
					currentNodeNum, currentNodeHost, currentNodePort);
			NodeServer.log(ctx, text);
			incrementVectorClock();
			@SuppressWarnings("unchecked")
			Vector<Integer> currentVectorClock = (Vector<Integer>) vectorClock
					.clone();
			NodeServer.logCriticalSection(ctx, text, currentVectorClock);
			/*
			 * If we leave the critical section, the next request in queue
			 * should be a forward request.
			 */
			TokenRequest nextRequest = deQueue();
			if (nextRequest != null) {
				nextRequest.response(ctx);
			}
			csSem.release();
		}
	}

	@Override
	public synchronized void acquireToken(Vector<Integer> receivedVectorClock, int fromNodeNum) {
		String fromNodeHost = ctx.getNodeHost(fromNodeNum);
		int fromNodePort = ctx.getNodePort(fromNodeNum);
		
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		
		String text = String.format("[SysMutex] Receiving token from Node %d(%s:%d) at Node %d(%s:%d)\n", 
				fromNodeNum, fromNodeHost, fromNodePort,
				currentNodeNum, currentNodeHost, currentNodePort);
		NodeServer.log(ctx, text);
		
		mergeVectorClock(receivedVectorClock);
		incrementVectorClock();
		setTokenOwner(true);
		setTokenParent(-1);
		/* The token request in here might be currentTask or forward */
		TokenRequest nextRequest = deQueue();
		if (nextRequest != null) {
			nextRequest.response(ctx);
		}
	}

	@Override
	public synchronized void claimToken(int claimerNodeNum, Vector<Integer> receivedVectorClock) {
		mergeVectorClock(receivedVectorClock);
		incrementVectorClock();
		int currentNodeNum = ctx.getCurrentNodeNum();
		if (claimerNodeNum == currentNodeNum) {
			throw new IllegalArgumentException("claimerNodeNum "
					+ claimerNodeNum + "should not equal to currentNodeNum");
		}
		
		int fromNodeNum = claimerNodeNum;
		String fromNodeHost = ctx.getNodeHost(fromNodeNum);
		int fromNodePort = ctx.getNodePort(fromNodeNum);
		
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		
		String text = String.format("[SysMutex] Receiving token request from Node %d(%s:%d) at Node %d(%s:%d)\n", 
				fromNodeNum, fromNodeHost, fromNodePort,
				currentNodeNum, currentNodeHost, currentNodePort);
		NodeServer.log(ctx, text);

		if (isTokenOwner) {
			if (isTokenUsing) {
				ForwardTokenRequest request = new ForwardTokenRequest(
						claimerNodeNum);
				enQueue(request);
			} else {
				forwardToken(claimerNodeNum);
			}
		} else {
			ForwardTokenRequest request = new ForwardTokenRequest(
					claimerNodeNum);
			enQueue(request);
			if (!isSentBefore()) {
				Commander commander = ctx.getCommander();
				incrementVectorClock();
				@SuppressWarnings("unchecked")
				Vector<Integer> clone = (Vector<Integer>) vectorClock.clone();
				commander.requestToken(getTokenParent(), ctx, clone);
				setSentBefore(true);
			}
		}
	}

	private void forwardTokenUnsafe(int dstNodeNum) {
		/* This function will be invoked in leave() and reponse() in ForwardTokenRequest.
		 * Hence, before we deQueue any "TokenRequest" and execute its response(),
		 * we need add lock to make sure the execution of response() must be atomic!!
		 * Besides, we need to make sure when we forward the token to others, 
		 * the isTokenUsing must be false, because we can't forward a token which is using.*/
		if (isTokenUsing) {
			throw new IllegalStateException("We can't forward a token which is using!!");
		}
		
		setTokenOwner(false);
		setTokenParent(dstNodeNum);

		Commander commander = ctx.getCommander();
		incrementVectorClock();
		@SuppressWarnings("unchecked")
		Vector<Integer> clone = (Vector<Integer>) vectorClock.clone();
		commander.requestTokenAck(dstNodeNum, ctx, clone);
		setSentBefore(false);
		
		if (!tokenRequestQueue.isEmpty()) {
			incrementVectorClock();
			@SuppressWarnings("unchecked")
			Vector<Integer> clone1 = (Vector<Integer>) vectorClock
					.clone();
			commander.requestToken(tokenParent, ctx, clone1);
			setSentBefore(true);
		}
	}
	
	@Override
	public synchronized void forwardToken(int dstNodeNum) {
		forwardTokenUnsafe(dstNodeNum);
	}

	@Override
	public void init(int ownerNodeNum) {
		int currentNodeNum = ctx.getCurrentNodeNum();
		
		if (ownerNodeNum != currentNodeNum) {
			setTokenParent(ownerNodeNum);
		} else {
			setTokenOwner(true);
		}
	}
	
	@Override
	public void setTokenUsing(boolean isUsing) {
		this.isTokenUsing = isUsing;
	}
}
