package proj2;

public class ReceiveTokenRequestAction implements Action {
	private ServerContext ctx;
	
	ReceiveTokenRequestAction(ServerContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void does(ParcelInfo info, Message message) {
		RaymondTokenManager tokenManager = ctx.getTokenManager();
		tokenManager.claimToken(info.getFromNodeNum(), message.getVectorClock());
	}
}
