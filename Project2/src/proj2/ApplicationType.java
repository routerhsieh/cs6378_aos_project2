package proj2;

public enum ApplicationType {
	BUILD_TREE("BuildTree"),
	BROADCAST("Broadcast"),
	REMOVE_TREE("RemoveTree"),
	SHUTDOWN("Shutdown"),
	ASSIGN_TOKEN("Token"),
	AUTO_DUMMY("AutoDummy"),
	AUTO_BROADCAST("AutoBroadcast");
	
	String name;
	ApplicationType(String name) {this.name = name;}
	@Override
	public String toString() {return name;}
}
