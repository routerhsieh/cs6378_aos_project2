package proj2;
/**
 * 
 * @author router
 *
 */
import java.util.*; 

public class ParcelHandler implements Communicator.ParcelReceiver, ActionRegistry {
	private Map<ParcelType, Action> ActionMap = null;
	private Communicator communicator = null;
	
	ParcelHandler (Communicator communicator) {
		this.ActionMap = new HashMap<ParcelType, Action>();
		this.communicator = communicator;
	}
	
	public void setCommunicator(Communicator communicator) {
		this.communicator = communicator;
	}
	
	public Action getAction(ParcelType type) {
		return ActionMap.get(type);
	}
	
	public void receive(Parcel parcel) {
		ParcelType type = parcel.getType();
		ParcelInfo info = parcel.getInfo();
		Message message = parcel.getMessage();
		
		Action action = ActionMap.get(type);
		action.does(info, message);
	}
	
	public void send(Parcel parcel) {
		communicator.send(parcel);
	}
	
	public void stopServer() {
		communicator.close();
	}
	
	public void register(Action action, ParcelType type) { 
			ActionMap.put(type, action);
	}
}
