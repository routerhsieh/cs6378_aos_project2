package proj2;

public class CurrentTaskTokenRequest implements TokenRequest {
	private volatile boolean isTokenAcquired;
	
	CurrentTaskTokenRequest(boolean isTokenAcquired) {
		this.isTokenAcquired = isTokenAcquired;
	}
	
	public void await() {
		while(!isTokenAcquired) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				if (isTokenAcquired) {
					break;
				}
			}
		}//end of while loop
	}
	
	@Override
	public void response(ServerContext ctx) {
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		String text = String.format("[SysMutex] Receiving token at Node %d(%s:%d), going to leave the loop in await() \n", 
				currentNodeNum, currentNodeHost, currentNodePort);
		NodeServer.log(ctx, text);
		
		
		/* If this method is called, then we are going to let app execute the critical section */
		RaymondTokenManager tokenManager = ctx.getTokenManager();
		tokenManager.setTokenUsing(true);
		this.isTokenAcquired = true;
	}

}
