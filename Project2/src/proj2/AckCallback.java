package proj2;

public interface AckCallback {
	void doAfterAllAcknowledged();
	void doAfterAcknowledged(ParcelInfo info, Message message);
	
	public class Default implements AckCallback{
		//Empty Listener
		public void doAfterAllAcknowledged() {}
		public void doAfterAcknowledged(ParcelInfo info, Message message) {}
	}
}
