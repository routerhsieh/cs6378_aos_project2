package proj2;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractReceiveBroadcastAction implements Action {

	protected ServerContext ctx;
	protected ParcelType outgoingType;

	public AbstractReceiveBroadcastAction() {
		super();
	}

	protected void sendAcknowledge(NodeInfo srcNode, NodeInfo dstNode, Message message,
			ParcelType outgoingType) {
				int srcNodeNum = srcNode.getNodeNum();
				
				int dstNodeNum = dstNode.getNodeNum();
				String dstHost = dstNode.getHost();
				int dstPort = dstNode.getPort();
				
				ParcelInfo info = new ParcelInfo(srcNodeNum, dstNodeNum);
				info.setDstHost(dstHost);
				info.setDstPort(dstPort);
				Parcel parcel = new Parcel(outgoingType, info, message);
				ctx.getHandler().send(parcel);
			}

	protected abstract void doAfterAckUpStream(); 
	
	protected void doAfterAllDownStreamAcknowledged(Conversation conversation, ParcelType outgoingType, ParcelType incomingType) {
				ParcelHandler handler = ctx.getHandler();
			
				int currentNodeNum = ctx.getCurrentNodeNum();
				NodeInfo currentNode = ctx.getCurrentNodeInfo();
				String currentHost = currentNode.getHost();
				int currentPort = currentNode.getPort();
				boolean notInitiator = !conversation.isInitiator();
			
				String doneMessage = String.format("%s done at Node %d(%s:%d)\n", incomingType.toString(), currentNodeNum, currentHost, currentPort);
				NodeServer.log(ctx, doneMessage);;
			
				if (notInitiator) {
					int dstNodeNum = conversation.getUpperStream();
					String dstHost = ctx.getNodeHost(dstNodeNum);
					int dstPort = ctx.getNodePort(dstNodeNum);
			
					ParcelInfo ackInfo = new ParcelInfo(currentNodeNum, conversation.getUpperStream());
					ackInfo.setDstHost(dstHost);
					ackInfo.setDstPort(dstPort);
			
					String ackText = String.format("%s: reply from %s:%d\n", outgoingType.toString(), currentHost, currentPort);
					Message message = new Message();
					message.setText(ackText);
					message.setOriginalType(outgoingType);
					message.setConversationId(conversation.getUuid());
			
					Parcel ackParcel = new Parcel(outgoingType, ackInfo,
							message);
					handler.send(ackParcel);
				}
				
				doAfterAckUpStream(); 
			}

	protected abstract void doBeforBroadCast(Conversation conversation, ParcelInfo info);
	
	protected void actionEnding(ParcelInfo info, Message message) {
		NodeInfo currentNode = ctx.getCurrentNodeInfo();
		NodeInfo upperStreamNode = ctx.getNodeInfo(info.getFromNodeNum());
		ParcelType originalType = message.getOriginalType();
		
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentHost = ctx.getCurrentHost();
		int currentPort = ctx.getCurrentPort();
		
		sendAcknowledge(currentNode, upperStreamNode, message, outgoingType);
		String doneMessage = String.format("%s done at Node %d(%s:%d)\n", originalType.toString(), currentNodeNum, currentHost, currentPort);
		NodeServer.log(ctx, doneMessage);
		
		ctx.endConversation(message.getConversationId());
		
		doAfterAckUpStream();
	}
	
	protected void actionReBroadCast(ParcelInfo info, Message message, final Conversation conversation) {
		int currentNodeNum = ctx.getCurrentNodeNum();
		final ParcelType incomingType = message.getOriginalType();
		
		Set<Integer> downStreams = new HashSet<Integer>(ctx.getLogicalNeighbors());
		downStreams.remove(info.getFromNodeNum());
		conversation.setUpperStream(info.getFromNodeNum());
		conversation.setDownStreams(downStreams);
		
		for (Integer downStreamNodeId : conversation.getDownStreams()) {
			String downStreamHost = ctx.getNodeHost(downStreamNodeId);
			int downStreamPort = ctx.getNodePort(downStreamNodeId);
			
			ParcelInfo bcastInfo = new ParcelInfo(currentNodeNum, downStreamNodeId);
			bcastInfo.setDstHost(downStreamHost);
			bcastInfo.setDstPort(downStreamPort);
			
			Parcel parcel = new Parcel(message.getOriginalType(), bcastInfo, message);
			conversation.setCallback(new AckCallback.Default(){

				@Override
				public void doAfterAllAcknowledged() {
					doAfterAllDownStreamAcknowledged(conversation, outgoingType, incomingType);
				} 
				
			});
			ctx.getHandler().send(parcel);
		}
	}
	
	@Override
	public void does(ParcelInfo info, Message message) {
		/*
		 * 1. Print the received message. 
		 * 2. Get the ParcelHandler and tree neighbors from the server context(ctx) 
		 * 3. Get Communicator through ParcelHandler for send(). 
		 * 4. If the sender is the only tree neighbor, then send BACK message to sender. 
		 * 5. Otherwise, re-broadcast the message to tree neighbor, except the sender of the message
		 */
		HashMap<Integer, NodeInfo> nodeInfos = ctx.getNodeInfos();
		Set<Integer> logicalNeighbors = ctx.getLogicalNeighbors();
		
		int upperStreamNodeNum = info.getFromNodeNum();
		NodeInfo upperStreamNode = nodeInfos.get(upperStreamNodeNum);
		String upperStreamHost = upperStreamNode.getHost();
		int upperStreamPort = upperStreamNode.getPort();
		
		NodeInfo currentNode = ctx.getCurrentNodeInfo();
		int currentNodeNum = currentNode.getNodeNum();
		
		if (logicalNeighbors.isEmpty()) {
			String emptyMessage = String.format("Node %d is not in spanning tree, please build it first\n", currentNodeNum);
			NodeServer.log(ctx, emptyMessage);
			return;
		}
		
		Conversation conversation = ctx.getConversation(message.getConversationId());
		if (conversation == null) {
			conversation = ctx.newConversation(message.getConversationId());
			Set<Integer> downStreams = new HashSet<Integer>(logicalNeighbors);
			downStreams.remove(info.getFromNodeNum());
			conversation.setUpperStream(info.getFromNodeNum());
			conversation.setDownStreams(downStreams);
		}
		
		ParcelType incomingType = message.getOriginalType();
		boolean notInitiator = !conversation.isInitiator();
		
		if (notInitiator) {
			String receiveMessage = String.format("Node %d: Receiving message [%s] from node %d(%s:%d)\n", currentNodeNum, message.getText(), upperStreamNodeNum,upperStreamHost, upperStreamPort);
			NodeServer.log(ctx, receiveMessage);
		}
		else {
			String sendMessage = String.format("Node %d: Sending %s message [%s]\n", currentNodeNum, incomingType.toString(), message.getText());
			NodeServer.log(ctx, sendMessage);
		}
		
		doBeforBroadCast(conversation, info);
	
		if (logicalNeighbors.size() == 1 && notInitiator) { 
			//handle leaf situation, because AckAction won't be trigger(no downStreams)
			actionEnding(info, message);
		} else {
			actionReBroadCast(info, message, conversation);
		}
	}

}