package proj2;

import java.io.Serializable;

public class ParcelInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int fromNodeNum;
	private int dstNodeNum;
	private String dstHost;
	private int dstPort;
	
	ParcelInfo(int srcNodeNum, int dstNodeNum) {
		this.fromNodeNum = srcNodeNum;
		this.dstNodeNum = dstNodeNum;
	}
	
	public int getFromNodeNum() {
		return fromNodeNum;
	}
	public void setFromNodeNum(int srcNodeNum) {
		this.fromNodeNum = srcNodeNum;
	}
	public int getDstNodeNum() {
		return dstNodeNum;
	}
	public void setDstNodeNum(int dstNodeNum) {
		//TODO: change Dst to To
		this.dstNodeNum = dstNodeNum;
	}

	public String getDstHost() {
		return dstHost;
	}

	public void setDstHost(String dstHost) {
		this.dstHost = dstHost;
	}

	public int getDstPort() {
		return dstPort;
	}

	public void setDstPort(int dstPort) {
		this.dstPort = dstPort;
	}
}
