package proj2;

public class ReceiveDisjoinAction extends AbstractReceiveBroadcastAction {
	
	ReceiveDisjoinAction(ServerContext ctx) {
		this.ctx = ctx;
		this.outgoingType = ParcelType.DISJOIN_ACK;
	}
	
	@Override
	protected void doAfterAckUpStream() {
		ctx.clearLogicalNeighbors();
		ctx.setInTree(false);
	}

	@Override
	protected void doBeforBroadCast(Conversation conversation, ParcelInfo info) {}

}
