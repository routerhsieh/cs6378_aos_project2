package proj2;

public class ApplicationAction implements Action {
	private ServerContext ctx;
	
	ApplicationAction(ServerContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void does(ParcelInfo info, Message message) {
		String appText = message.getText();
		ApplicationType appType = message.getAppType();
		Applications app = new Applications(ctx);
		
		switch (appType) {
		case BUILD_TREE:
			app.builTreeApp(appText);
			break;
		case BROADCAST:
			app.broadcastApp(appText);
			break;
		case REMOVE_TREE:
			app.destroyTreeApp(appText);
			break;
		case SHUTDOWN:
			app.shutdownApp(appText);
			break;
		case ASSIGN_TOKEN:
			app.assignTokenApp(appText);
			break;
		case AUTO_DUMMY:
			app.autoDummyApp();
			break;
		case AUTO_BROADCAST:
			app.autoBroadcastApp(appText);
			break;
		default:
			break;
		}
	}

}
