package proj2;

public class ReceiveTokenRequestAckAction implements Action {
	private ServerContext ctx;
	
	ReceiveTokenRequestAckAction (ServerContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void does(ParcelInfo info, Message message) {		
		RaymondTokenManager tokenManager = ctx.getTokenManager();
		tokenManager.acquireToken(message.getVectorClock(), info.getFromNodeNum());
	}
}
