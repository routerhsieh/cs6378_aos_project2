package proj2;

import java.io.IOException;
import java.io.*;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class CommunicatorImpl implements Communicator {
	private volatile boolean Running = true;
	private int port = 0;
	private final Thread ServerThread;
	private ServerSocket serverSocket = null;
	private ParcelReceiver parcelReceiver;
	private ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
	
	public CommunicatorImpl(int port) {
		super();
		this.port = port;
		
		ServerThread = new Thread(new Runnable(){

			@Override
			public void run() {
				awaitClientSendParcel();
			}});
		
		ServerThread.start();
	}
	
	public void close() {
		this.Running = false;
		try {
			this.serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void doReceive(final Parcel parcel) {
		threadPool.execute(new Runnable(){

			@Override
			public void run() {
				try {
					parcelReceiver.receive(parcel);
				}
				catch (RuntimeException ex) {
					ex.printStackTrace();
				}
			}});
	}
	
	private void doShutDownThreadPool() {
		threadPool.shutdown();
	}
	
	private void doShutDownObjectInput(ObjectInput in) {
		if (in != null) {
			try {
				in.close();
			}
			catch (IOException ex) {
				System.out.println("Can't close object input stream properly!!");
				ex.printStackTrace();
			}
		}
	}
	
	private void doShutDownServerSocket(ServerSocket server) {
		if (server != null) {
			try {
				server.close();
			}
			catch (IOException ex) {
				System.out.println("Can't close server socket proeprly!!");
				ex.printStackTrace();
			}
		}
	}
	
	private void awaitClientSendParcel() {
		/* This is where we running our server thread(main loop) */
		try {
			serverSocket = new ServerSocket(port);
		}
		catch (IOException ex) {
			System.out.printf("Can't open server socket on port %d!!\n", port);
			ex.printStackTrace();
		}
		
		Socket clientSocket = null;
		ObjectInput in = null;
		
		while (Running) {
			try {
				clientSocket = serverSocket.accept();
				in = new ObjectInputStream(clientSocket.getInputStream());
				Parcel received = (Parcel) in.readObject();
				clientSocket.close();
				in.close();
				doReceive(received);
			}
			catch (ClassNotFoundException | IOException ex) {
				if (!Running) {
					break;
				}
				throw new RuntimeException("Error accepting client connection", ex);
			}
		}//end of while loop
		doShutDownThreadPool();
		doShutDownObjectInput(in);
		doShutDownServerSocket(serverSocket);
		System.out.println("Server Stopped!!");
	}

	@Override
	public void send(Parcel parcel) {
		// 1. get dst_host, dst_port from parcel
		// 2. open remote server socket and write object out
		ParcelInfo info = parcel.getInfo();
		String dst_host = info.getDstHost();
		int dst_port = info.getDstPort();
		Socket clientSocket = null;
		ObjectOutput out = null;
		
		try {
			clientSocket = new Socket(dst_host, dst_port);
			out = new ObjectOutputStream(clientSocket.getOutputStream());
			out.writeObject(parcel);
			out.flush();
		}
		catch (IOException ex) {
			System.out.printf("Can't open client socket with %s:%d properly!!\n", dst_host, dst_port);
			ex.printStackTrace();
		}
		finally {
			if (out != null) {
				try {
					out.close();
				}
				catch (IOException ex) {
					System.out.println("Can't close the object output stream properly!!");
					ex.printStackTrace();
				}
			}
			if (clientSocket != null) {
				try {
					clientSocket.close();
				}
				catch (IOException ex) {
					System.out.println("Can't close the client socket properly!!");
					ex.printStackTrace();
				}
			}
		}
		
	}

	@Override
	public void setReceiver(ParcelReceiver receiver) {
		this.parcelReceiver = receiver;
	}
}
