package proj2;

import java.util.Vector;

public class Commander {
	private void buildTokenParcel(int dstNodeNum, ServerContext ctx, ParcelType tokenType, String tokenText, Vector<Integer> currentVectorClock) {
		int currentNodeNum = ctx.getCurrentNodeNum();
		String dstNodeHost = ctx.getNodeHost(dstNodeNum);
		int dstNodePort = ctx.getNodePort(dstNodeNum);
		
		ParcelInfo info = new ParcelInfo(currentNodeNum, dstNodeNum);
		info.setDstHost(dstNodeHost);
		info.setDstPort(dstNodePort);
		
		Message message = new Message(currentNodeNum, tokenText, tokenType);
		message.setVectorClock(currentVectorClock);
		Parcel parcel = new Parcel(tokenType, info, message);
		ctx.getHandler().send(parcel);
		NodeServer.log(ctx, tokenText);
	}
	
	public void requestToken(int dstNodeNum, ServerContext ctx, Vector<Integer> currentVectorClock) {
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		
		String dstNodeHost = ctx.getNodeHost(dstNodeNum);
		int dstNodePort = ctx.getNodePort(dstNodeNum);
		
		String text = String.format("[SysMutex] Sending Token Request from Node %d(%s:%d) to Node %d(%s:%d)\n", 
				currentNodeNum, currentNodeHost, currentNodePort, 
				dstNodeNum, dstNodeHost, dstNodePort);
		
		buildTokenParcel(dstNodeNum, ctx, ParcelType.REQ_TOKEN, text, currentVectorClock);
	}
	
	public void requestTokenAck(int dstNodeNum, ServerContext ctx, Vector<Integer> currentVectorClock) {
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentNodeHost = ctx.getCurrentHost();
		int currentNodePort = ctx.getCurrentPort();
		
		String dstNodeHost = ctx.getNodeHost(dstNodeNum);
		int dstNodePort = ctx.getNodePort(dstNodeNum);
		
		String text = String.format("[SysMutex] Sending Token Request ACK from Node %d(%s:%d) to Node %d(%s:%d)\n", 
				currentNodeNum, currentNodeHost, currentNodePort, 
				dstNodeNum, dstNodeHost, dstNodePort);
		
		buildTokenParcel(dstNodeNum, ctx, ParcelType.REQ_TOKEN_ACK, text, currentVectorClock);
	}
	
	private void buildAppParcel(String appText, ServerContext ctx, ParcelType parcelType) {
		ParcelInfo info = new ParcelInfo(0, 0);
		Message message = new Message(0, appText, parcelType);
		ParcelHandler handler = ctx.getHandler();
		Action cmdAction = handler.getAction(ParcelType.CMD);
		cmdAction.does(info, message);
	}
	
	public void buildSpanningTree(String text, ServerContext ctx) {
		buildAppParcel(text, ctx, ParcelType.JOIN);
	}
	
	public void assignToken(String text, ServerContext ctx) {
		buildAppParcel(text, ctx, ParcelType.TOKEN_ASSIGN);
	}
	
	public void broadcastMessage(String text, ServerContext ctx) {
		buildAppParcel(text, ctx, ParcelType.BORADCAST);
	}
	
	public void shutdownServer(String text, ServerContext ctx) {
		buildAppParcel(text, ctx, ParcelType.SHUTDOWN);
	}
	
	public void destroySpanningTree(String text, ServerContext ctx) {
		buildAppParcel(text, ctx, ParcelType.DISJOIN);
	}
}
