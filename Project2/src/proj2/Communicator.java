package proj2;

/**
 * 1. send/subscribe methods
 * @author router
 *
 */
public interface Communicator {
	/**
	 * 
	 */
	interface ParcelReceiver{
		public void receive(Parcel parcel);
	}
	
	public void send (Parcel parcel);
	public void setReceiver (ParcelReceiver receiver);
	public void close();
	
}
