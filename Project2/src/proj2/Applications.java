package proj2;

import java.util.Random;

/**
 * This is the "Application Module" for this project
 * @author router
 *
 */

public class Applications {
	private ServerContext ctx;
	
	Applications (ServerContext ctx) {
		this.ctx = ctx;
	}
	
	public void builTreeApp(String text) {	
		Commander commander = ctx.getCommander();
		commander.buildSpanningTree(text, ctx);
	}
	
	public void assignTokenApp(String text) {
		Commander commander = ctx.getCommander();
		commander.assignToken(text, ctx);
	}
	
	public void broadcastApp(String text) {
		SystemMutex sysMutex = ctx.getSysMutex();
		Commander commander = ctx.getCommander();
		sysMutex.enter();
		commander.broadcastMessage(text, ctx);
		sysMutex.leave();
	}
	
	public void destroyTreeApp(String text) {
		SystemMutex sysMutex = ctx.getSysMutex();
		Commander commander = ctx.getCommander();
		sysMutex.enter();
		commander.destroySpanningTree(text, ctx);
		sysMutex.leave();
	}
	
	public void shutdownApp(String text) {
		SystemMutex sysMutex = ctx.getSysMutex();
		Commander commander = ctx.getCommander();
		sysMutex.enter();
		commander.shutdownServer(text, ctx);
		sysMutex.leave();
	}
	
	public void autoBroadcastApp(String text) {
		Random randomno = new Random();
		for (int idx = 0; idx < ctx.getAutoCSRequirements(); idx++) {
			broadcastApp(text);
			try {
				int meanCSRequirement = ctx.getMeanCSRequirementInterval();
				long sleep = (long) (randomno.nextGaussian() * (0.1 * meanCSRequirement) + meanCSRequirement);
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}//end of for loop
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentHost = ctx.getCurrentHost();
		int currentPort = ctx.getCurrentPort();
		String doneText = String.format("All CS requirements at Node %d(%s:%d) is done",
				currentNodeNum, currentHost, currentPort);
		NodeServer.log(ctx, doneText);
	}
	
	public void dummyApp() {
		Random randomno = new Random();
		SystemMutex sysMutex = ctx.getSysMutex();
		String dummyText = String.format(
				"Node %d running in critical section for %d ms\n",
				ctx.getCurrentNodeNum(), ctx.getMeanCSRunningInterval());
		
		sysMutex.enter();
		NodeServer.log(ctx, dummyText);
		try {
			int meanCSRunning = ctx.getMeanCSRunningInterval();
			long sleep = (long) (randomno.nextGaussian()*(0.1 * meanCSRunning) + meanCSRunning);
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		sysMutex.leave();
	}
	
	public void autoDummyApp() {
		Random randomno = new Random();
		for (int idx = 0; idx < ctx.getAutoCSRequirements(); idx++) {
			dummyApp();
			try {
				int meanCSRequirement = ctx.getMeanCSRequirementInterval();
				long sleep = (long) (randomno.nextGaussian() * (0.1 * meanCSRequirement) + meanCSRequirement);
				Thread.sleep(sleep);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}//end of for loop
		int currentNodeNum = ctx.getCurrentNodeNum();
		String currentHost = ctx.getCurrentHost();
		int currentPort = ctx.getCurrentPort();
		String text = String.format("All CS requirements at Node %d(%s:%d) is done",
				currentNodeNum, currentHost, currentPort);
		NodeServer.log(ctx, text);
	}
}
