package proj2;

public class ReceiveAcknowledgeAction implements Action {
	private ServerContext ctx;

	ReceiveAcknowledgeAction(ServerContext ctx) {
		this.ctx = ctx;
	}

	@Override
	public void does(ParcelInfo info, Message message) {
		Conversation conversation = ctx.getConversation(message.getConversationId());
		conversation.acknowledge(info, message);
		if (conversation.isAllAcknowledged()) {
			AckCallback doAfterAllCallback = conversation.getCallback();
			doAfterAllCallback.doAfterAllAcknowledged();
			ctx.endConversation(conversation.getUuid());
		}
	}
}
