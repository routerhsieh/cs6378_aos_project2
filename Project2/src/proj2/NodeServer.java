package proj2;

import java.io.*;
import java.text.*;
import java.util.*;

/**
 * 1. Entry point of whoal implementation
 * 2. Parse configuration file
 * 3. Module initialization
 * @author router
 *
 */
public class NodeServer {
	
	public static synchronized void log(ServerContext ctx, String originalMessage) {
		String outputFilePath = ctx.getOutputFilePath();
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss:SS");
		String outputMessage = String.format("\n<%s>: %s",
				dateFormat.format(new Date()), originalMessage);
		File outputFile = new File(outputFilePath);
		if (!outputFile.exists()) {
			try {
				outputFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			Writer writer = new FileWriter(outputFile, true);
			writer.write(outputMessage);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.print(outputMessage);
	}
	
	public static synchronized void logCriticalSection(ServerContext ctx, String originalMessage, Vector<Integer> vectorClock) {
		String outputFilePathCriticalSection = ctx.getOutputFilePathCriticalSection();
		String outputFilePath = ctx.getOutputFilePath();
		String outputMessage = String.format("\n%s\t%s", vectorClock, originalMessage);
		File outputFileCriticalSection = new File(outputFilePathCriticalSection);
		File outputFile = new File(outputFilePath);
		
		if (!outputFileCriticalSection.exists()) {
			try {
				outputFileCriticalSection.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		try {
			Writer writer = new FileWriter(outputFileCriticalSection, true);
			writer.write(outputMessage);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (!outputFile.exists()) {
			try {
				outputFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			Writer writer = new FileWriter(outputFile, true);
			writer.write(outputMessage);
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.print(outputMessage);
	}
	
	private static void buildNodeMapping(int nodeNum, String[] parameter, HashMap<Integer, NodeInfo> nodeInfos) {
		String host = parameter[0] + ".utdallas.edu";
		int port = Integer.parseInt(parameter[1]);
		NodeInfo info = new NodeInfo(nodeNum, host, port);
		Set<Integer> neighbors = info.getPhysicalNeighbors();
		for (int i = 2; i < parameter.length; i++) {
			neighbors.add(Integer.parseInt(parameter[i]));
		}
		nodeInfos.put(nodeNum, info);
	}
	
	private static void parseConfigFile(String file_path, HashMap<Integer, NodeInfo> nodeInfos) {
		File file = new File(file_path);
		int nodeNum = 1;
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (!line.isEmpty()) {
					char c = line.charAt(0);
					if (Character.isLetter(c)) {
						/* The '\s' means a string, so we need one more back slash('\') in front of '\s', 
						 * and '+' means one or more strings, that's a regular expression */
						String[] parameter = line.split("\\s+");
						buildNodeMapping(nodeNum, parameter, nodeInfos);
						nodeNum += 1;
					}
				}
			}//end of while loop
		}
		catch (FileNotFoundException ex) {
			System.out.printf("File %s is not found!!\n", file_path);
		}
		finally {
			if (scanner != null) {
				scanner.close();
			}
		}
	}
	
	//TODO: Add Server Close method in this class
	
	public static void main (String[] args) {
		//TODO 
		//1. Parse Configruation
		//2. Prepare ServerContext
		//3. Moudle initialization
		/* We should pass the configuration file path and node number through command line argument */
		if (args.length < 3) {
			System.out.println("Usage: java proj1 config.txt NodeNumber output.log");
			return;
		}
		
		String intputFilePath = args[0];
		String outputFilePath = args[2];
		int nodeNumber = Integer.parseInt(args[1]);
		HashMap<Integer, NodeInfo> nodeInfos = new HashMap<Integer, NodeInfo>();
		parseConfigFile(intputFilePath, nodeInfos);
		NodeInfo node = nodeInfos.get(nodeNumber);
		int local_port = node.getPort();
		
		
		Communicator communicator = new CommunicatorImpl(local_port);
		ParcelHandler handler = new ParcelHandler(communicator);
		ServerContext ctx = new ServerContext(handler, nodeNumber, nodeInfos, outputFilePath);
		SystemMutexRaymond sysMutex = new SystemMutexRaymond(ctx);
		ctx.setSysMutex(sysMutex);
		ctx.setTokenManager(sysMutex);
		
		if (args.length > 3 && args.length < 7) {
			ctx.setAutoCSRequirements(Integer.parseInt(args[3]));
			ctx.setMeanCSRequirementInterval(Integer.parseInt(args[4]));
			ctx.setMeanCSRunningInterval(Integer.parseInt(args[5]));
		}
		
		handler.register(new ReceiveJoinAction(ctx), ParcelType.JOIN);
		
		handler.register(new ReceiveTokenAssignAction(ctx), ParcelType.TOKEN_ASSIGN);
		handler.register(new ReceiveBroadcastAction(ctx), ParcelType.BORADCAST);
		handler.register(new ReceiveShutdownAction(ctx), ParcelType.SHUTDOWN);
		handler.register(new ReceiveDisjoinAction(ctx), ParcelType.DISJOIN);
		
		Action receiveAcknowledgeAction = new ReceiveAcknowledgeAction(ctx);
		handler.register(receiveAcknowledgeAction, ParcelType.BROADCAST_ACK);
		handler.register(receiveAcknowledgeAction, ParcelType.TOKEN_ASSIGN_ACK);
		handler.register(receiveAcknowledgeAction, ParcelType.JOIN_ACK);
		handler.register(receiveAcknowledgeAction, ParcelType.NON_JOIN_ACK);
		handler.register(receiveAcknowledgeAction, ParcelType.DISJOIN_ACK);
		handler.register(receiveAcknowledgeAction, ParcelType.SHUTDOWN_ACK);
		
		handler.register(new ReceiveTokenRequestAction(ctx), ParcelType.REQ_TOKEN);
		handler.register(new ReceiveTokenRequestAckAction(ctx), ParcelType.REQ_TOKEN_ACK);
		
		handler.register(new CmdAction(ctx), ParcelType.CMD);
		handler.register(new ApplicationAction(ctx), ParcelType.APP);
		
		communicator.setReceiver(handler);
		
		String text = String.format("NodeServer %d at %s:%d start\n", nodeNumber, ctx.getCurrentHost(), ctx.getCurrentPort() );
		NodeServer.log(ctx, text);
	}
	
	/*
	class CommunicatorDummy implements Communicator{
		 private ParcelReceiver receiver;
		 public void send(Parcel parcel){
		  receiver.receive(parcel);
		 }
		 
		 public void subscribe(ParcelReceiver receiver){
		  this.receiver = receiver;
		 }
		}
	*/
}
