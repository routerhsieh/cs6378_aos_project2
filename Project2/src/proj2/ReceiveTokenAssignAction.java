package proj2;

public class ReceiveTokenAssignAction extends AbstractReceiveBroadcastAction {
	
	ReceiveTokenAssignAction(ServerContext ctx) {
		this.ctx = ctx;
		this.outgoingType = ParcelType.TOKEN_ASSIGN_ACK;
	}
	
	@Override
	protected void doAfterAckUpStream() {}

	@Override
	protected void doBeforBroadCast(Conversation conversation, ParcelInfo info) {
		int tokenParentNum;
		boolean notInitiator = !conversation.isInitiator();
		
		if (notInitiator) {
			tokenParentNum = info.getFromNodeNum();
		}
		else {
			tokenParentNum = ctx.getCurrentNodeNum();
		}
		ctx.getTokenManager().init(tokenParentNum);
	}

}
