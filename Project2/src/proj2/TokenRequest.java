package proj2;

/**
 * 1. There are two types of token request now. One is CurrentTaskTokenRequest, the other is ForwardTokenRequest
 * 2. All token request will use response() to invoke relative Actions
 * @author router
 *
 */
public interface TokenRequest {
	void response(ServerContext ctx);
}
